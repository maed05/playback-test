
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;import helper.DbHelper;
import helper.GetAvgResponseTime;
import helper.RegexMatcher;
import helper.StoreNewResponse;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.URL;
import static org.junit.Assert.*;
import org.testng.ITestResult;
import org.junit.runners.model.Statement;
import org.junit.runners.model.FrameworkMethod;
import java.io.FileOutputStream;


public class PlaybackResponseTest {
    //private WebDriver driver;
    private int limit = Integer.parseInt(System.getProperties().getProperty("timeLimit").toString());
    private String userName = System.getProperties().getProperty("userName").toString();
    private String password = System.getProperties().getProperty("password").toString();
    private String baseUrl = System.getProperties().getProperty("baseUrl").toString();
    private int environment = Integer.parseInt(System.getProperties().getProperty("environment").toString());
    //private String browser = System.getProperties().getProperty("browser").toString();
    private double avgResponseTime;
    private double seconds;
    private RemoteWebDriver driver;
    private String browser = "BrowserNotSet";
   // private RemoteWebDriver driver = new RemoteWebDriver();

    @Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();

    @Before
    public void setUp() throws Exception {
        //Get previous Response times Average
        avgResponseTime = GetAvgResponseTime.getTime(environment);
        System.out.println("Avg. response Time: "+ round(avgResponseTime));


    }

    @Test
    public void loginAndLoadPlaybackFirefox() throws Exception {
        double seconds = ConnectToPlayBack("firefox");
        assertTrue("Making sure that we have load time lower than the set limit", seconds<limit);
        assertTrue("Making sure that we have load time lower or equal to the last 5 run average duration + 2 seconds", seconds<=(avgResponseTime+2));
    }

   /* @Test
    public void loginAndLoadPlaybackChrome() throws Exception {
        double seconds = ConnectToPlayBack("chrome");
        assertTrue("Making sure that we have load time lower than the set limit", seconds<limit);
        assertTrue("Making sure that we have load time lower or equal to the last 5 run average duration + 2 seconds", seconds<=(avgResponseTime+2));
    }
*/

    public double ConnectToPlayBack(String browsers) throws Exception{
        String testStartTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()).toString();
        browser = browsers;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(browser);

        driver = new RemoteWebDriver(new URL("http://svt-stoprod-seleniumgrid02:4444/wd/hub"), capabilities);



//        RemoteWebDriver driver = new RemoteWebDriver(new URL("http://10.20.124.141:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.get(baseUrl);

        driver.manage().window().setSize(new Dimension(1024, 768));

        driver.findElement(By.cssSelector("input.form-control")).sendKeys(userName);
        driver.findElement(By.xpath("//input[@type='password']")).sendKeys(password);
        driver.findElement(By.xpath("/html/body/div/div/div[2]/div/div/form/button")).click();
        long start = System.currentTimeMillis();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        assertTrue("Could not find loader on page",driver.findElement(By.xpath("//*[@id=\"loader\"]")).isDisplayed());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\"loader\"]")));

        double finish = System.currentTimeMillis();
        double totalTime = finish - start;

        seconds = round((totalTime / 1000));
        System.out.println("Total Time for page load - " + seconds);

        StoreNewResponse.Store(environment, testStartTime, seconds, browser);

        if(seconds>limit){
            //fail("Test Failed: It took " + seconds + " seconds to load page. Limit is set to " + limit + " seconds.");
            System.out.println("Test Failed: It took " + seconds + " seconds to load page. Limit is set to " + limit + " seconds.");
        }

        //Assert present Image Objects in left lane
        assertTrue("Assert that image for svt1 in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(2) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());
        assertTrue("Assert that image for svt2 in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(3) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());
        assertTrue("Assert that image for Barnkanalen in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(4) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());
        assertTrue("Assert that image for Webbexklusivt in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(5) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());
        assertTrue("Assert that image for Live in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(6) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());
        assertTrue("Assert that image for svt24 in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(7) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());
        assertTrue("Assert that image for Kunskapskanalen in left lane is Displayed",driver.findElement(By.cssSelector("#sidebar > div > div.scrollable.styled-scroll > div.channels-sidebar > div:nth-child(8) > div.col-lg-6.col-md-6.col-sm-4.col-xs-3 > div")).isDisplayed());

        //Assert present Image Objects per column on  main page
        assertTrue("Assert that image for svt1 column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > header > h2 > span.channel-image")).isDisplayed());
        assertTrue("Assert that image for svt2 column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(2) > header > h2 > span.channel-image")).isDisplayed());
        assertTrue("Assert that image for Barnkanalen column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(3) > header > h2 > span.channel-image")).isDisplayed());
        assertTrue("Assert that image for Webbexklusivt column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(4) > header > h2 > span.channel-title")).isDisplayed());
        assertTrue("Assert that image for Live column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(5) > header > h2 > span.channel-image")).isDisplayed());
        assertTrue("Assert that image for svt24 column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(6) > header > h2 > span.channel-image")).isDisplayed());
        assertTrue("Assert that image for Kunskapskanalen column in main page Displayed",driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(7) > header > h2 > span.channel-image")).isDisplayed());

        //Assert present Objects for the first Program at SVT1
        String programName = driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div > div:nth-child(1) > div.col.program-title.col-lg-5.col-md-5.col-sm-5.col-xs-5 > div > p")).getText();
        driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div > div:nth-child(1) > div.col.program-title.col-lg-5.col-md-5.col-sm-5.col-xs-5 > div > p")).click();
        System.out.println("programName: " + programName);
        assertThat("Assert program name to include only letters", programName, RegexMatcher.matchesRegex("([A-Öa-ö '!?:0-9]+)"));

        String programEpisodeVersionId = driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div > div:nth-child(1) > div.col.program-title.col-lg-5.col-md-5.col-sm-5.col-xs-5 > div > input[type=\"text\"]")).getAttribute("value").toString();
        System.out.println("programEpisodeVersionId: " + programEpisodeVersionId);
        assertThat("Assert format of Program Episode Version Id", programEpisodeVersionId, RegexMatcher.matchesRegex("([0-9-]+[A-Z]+)"));

        driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div > div:nth-child(2) > div.col.program-additional.col-lg-1.col-md-1.col-sm-1.col-xs-1 > i")).click();

        String shortDescription = driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div.medium-card > div > div > div.tab-pane.fade.active.in > div:nth-child(1) > div:nth-child(1) > h5")).getText();
        assertEquals("Assert headline for program short description", "Kort beskrivning", shortDescription);

        String episodeInformation = driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div.medium-card > div > div > div.tab-pane.fade.active.in > div:nth-child(1) > div:nth-child(2) > h5")).getText();
        assertEquals("Assert headline for program episode information", "Episodinformation", episodeInformation);

        String sourceFile = driver.findElement(By.cssSelector("#program-area > div > div.channel-wrap.styled-scroll > div:nth-child(1) > div > div:nth-child(1) > div.medium-card > div > div > div.tab-pane.fade.active.in > div:nth-child(2) > div > h5")).getText();
        assertEquals("Assert headline for program source file", "Källfil", sourceFile);


        return seconds;
    }

    public static double round(double d) {
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }


    @After
    public void tearDown() throws Exception {
        System.out.println("\n\n");
    }

    class ScreenshotTestRule implements MethodRule {
        public Statement apply(final Statement statement, final FrameworkMethod frameworkMethod, final Object o) {
            return new Statement() {
                @Override
                public void evaluate() throws Throwable {
                    try {
                        statement.evaluate();
                    } catch (Throwable t) {
                        captureScreenshot(frameworkMethod.getName());
                        throw t; // rethrow to allow the failure to be reported to JUnit
                    }
                }

                public void captureScreenshot(String fileName) {
                    try {

                        fileName = new java.text.SimpleDateFormat("yyyy-MM-dd_HHmmss").format(new Date()).toString() + "-" + browser + " - " + fileName;
                        //System.out.println("WE ARE HERE: captureScreenshot " +fileName+".png should be created");
                        new File("target/screenShots/").mkdirs(); // Insure directory is there
                        FileOutputStream out = new FileOutputStream("target/screenShots/" + fileName + ".png");
                        out.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
                        out.close();
                    } catch (Exception e) {
                        // No need to crash the tests if the screenshot fails
                    }
                }
            };
        }
    }

}

