package helper;

import java.sql.*;
import java.util.*;



public class DbHelper {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:mysql://10.23.22.214/playback_stage";

    //  Database credentials
    static final String USER = System.getProperties().getProperty("dbUserName").toString();
    static final String PASS = System.getProperties().getProperty("dbPassword").toString();

    public Connection connect = null;
    public Statement statement = null;
    public ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    public List durations = new ArrayList<Integer>();

    public List selectStatement(String selectStatement) {

        //Class.forName("com.mysql.jdbc.Driver");
        Properties info = new Properties( );
        info.put( "user", USER );
        info.put( "password", PASS );

        try {
            connect = DriverManager.getConnection(DB_URL, info);
            if (connect != null) {
                System.out.println("Connected to the database");

                // Statements allow to issue SQL queries to the database
                statement = connect.createStatement();
                // Result set get the result of the SQL query
                resultSet = statement
                        .executeQuery(selectStatement);

                //durations = resultSet.getArray("duration").toString();
                //System.out.println("resultSet last: " + resultSet.last());
                //System.out.println("resultSet getRow: " + resultSet.getRow());

                while (resultSet.next()) {
                    //System.out.println("resultSet.getInt(\"duration\"): " + resultSet.getInt("duration"));
                    durations.add(resultSet.getInt("duration"));
                }

            }
        }catch(Exception e){
            System.out.println("Error during DB connection: " +e);
        } finally {
            close();
        }

    return durations;
    }

    public void InsertNewDuration(int environment,String testStartTime, double seconds, String browser){
        //Class.forName("com.mysql.jdbc.Driver");
        Properties info = new Properties( );
        info.put( "user", USER );
        info.put( "password", PASS );

        try {
            connect = DriverManager.getConnection(DB_URL, info);
            if (connect != null) {
                //System.out.println("Connected to the database");

                // PreparedStatements can use variables and are more efficient
                preparedStatement = connect
                        .prepareStatement("insert into test.PlaybackResponseTime values (default, ?, ?, ?, ?);");
                // "myuser, webpage, datum, summery, COMMENTS from feedback.comments");
                // Parameters start with 1
                preparedStatement.setInt(1, environment);
                preparedStatement.setString(2, testStartTime);
                preparedStatement.setDouble(3, seconds);
                preparedStatement.setString(4, browser);
                preparedStatement.executeUpdate();
                //System.out.println("Here we are");
            }
        }catch(Exception e){
            System.out.println("Error during DB connection for insert: " +e);
        } finally {
            close();
        }
    }

    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
            System.out.println("Error at closing Statement");
        }
    }
}
