package helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mats on 2015-10-21.
 */
public class GetAvgResponseTime extends DbHelper {
    public static double getTime(double environment){
        double avg=0;

        DbHelper connect = new DbHelper();
        List durations = new ArrayList<Integer>();
        durations = connect.selectStatement("select duration from test.PlaybackResponseTime where environment = "+environment+" order by id DESC LIMIT 5;");
        avg = calculateAverage(durations);

        return avg;

    }

    public static double calculateAverage(List<Integer> marks) {
        double sum = 0;

        for (int i=0; i< marks.size(); i++) {
            sum += marks.get(i);
        }
        return sum / marks.size();
    }
}
